# Codemetric with ML

This project describes the implementation part of Arl Robin Ferhati's master thesis.
The topic of this thesis was "Conceptualization and design of a code metric for dynamic source code analysis using machine learning.

## Getting started

For this repository the following requirements are necessary to run the code successfully:

- Python 3.7.0 or higher
- Tensorflow 2.9.0 or higher
- A good GPU is an advantage if a (re)training is requested (I used a NVIDIA GeForce GTX 1060 6GB)

## Intro

In this project, a newly defined code metric is calculated. The metric deals with the dynamic analysis of the written source code. <br/>
Since this is still a conceptual design and a draft, the processing times of the analysis still require some time.

To work with the repository, it is recommended to clone the repository locally.

## First implementation attempt:

1. Navigate to the "TestFileForMetric.cs" (codemetric-with-ml/app/data/test/).
2. Edit the file as desired.
3. Execute the shell file "startCalculation.sh" (codemetric-with-ml/app/scripts/).

This is sufficient for the first implementation attempt. Both a static and a dynamic source code analysis is performed. The corresponding log files in the terminal inform the user about the current intermediate status.

The computation of the name generation for the method name takes some time therefore a dynamic user interaction was inserted. This asks the user whether to adjust the file again or not. If one enters **"q"** or **"quit"** the analysis continues. If you type another letter, the method name is predicted. This happens _until_ the user ends the operation by typing "q" or "quit".

### Coding Behavior:

During the analysis, a .csv is created and documented in parallel with the development. This can be found under codemetric-with-ml/data/realtime/codingBehavior.csv. This shows the progress that was recorded at the time of the analysis. This table can also be viewed in the terminal during the analysis. The data frame is output before and after the value is determined.

### Fulfillment of the source code requirements:

The value **metricFulfillment** indicates which measure of this metric was fulfilled. It is a value which lies between 0 and 1. The closer it is to 1, the more criteria have been fulfilled. This indicates a well-written code or a code that adheres to and fulfills the coding conventions.
