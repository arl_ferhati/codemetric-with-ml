object f(int n, int target) {
    if (n <= 1) {
        return null;
    }
    for (int i = 2; i * i <= n; i++) {
        if (n % i == target) {
            return this.element;
        }
    }
    return this.defaultValue;
}

