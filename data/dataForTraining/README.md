# Note on handling this folder

This folder contains the test data sets for name generation. <br/>
These must go through the training and the corresponding number of epochs to generate a corresponding model. <br/>
_In the local run, the trained data is almost 30 GB in size. Gitlab and its commits are limited and therefore the trained model must be re-trained and run on the user's machine._

Another behavior is described in which the user can train a model by himself and use it accordingly for the prediction.
