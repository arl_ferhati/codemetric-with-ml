class MetricExceptionHandling(Exception):
    """Base class for other exceptions"""
    pass


class WrongNamingException(MetricExceptionHandling):
    """The corresponding naming convention was not observed

    Attributes:
        message -- Make sure to check naming convention or typos
    """

    def __init__(self, message="Name for unit test class does not match with file name"):
        self.message = message
        super().__init__(self.message)


class TestFolderMissingException(MetricExceptionHandling):
    """The corresponding test folder for the written src folder does not exist

    Attributes:
        message -- Make sure to adapt the code coverage and create the test folder
    """

    def __init__(self, message="Folder for unit test class does not exist for the src folder"):
        self.message = message
        super().__init__(self.message)


class UnitTestMissingException(MetricExceptionHandling):
    """The corresponding unit test for the analyed file is missing

    Attributes:
        message -- Make sure to adapt the code coverage
    """

    def __init__(self, message="Unit test class does not exist"):
        self.message = message
        super().__init__(self.message)
