import csv
import pathlib as pl
import os.path
import pandas as pd

# In this class the calculation of the metric takes place.
# Here the value between 0 and 1 is determined how many percent of the analysed controls have been fulfilled.
# The value is then communicated to the user.

# Idea on how to design the valuation of the metrics for the calculation

# UnittestCheck                 0.08
# MeaningfulNamesCheck          0.33
# LinesOfCodeCheck              0.06
# InheritanceCheck              0.10
# FunctionalityCheck            0.28
# CyclomaticComplexityCheck     0.15    For each low rank 0.3 points are deducted from the score


def GetActiveFileWithPath():
    fileName = "codingBehavior"
    fileFormat = ".csv"
    pathToMainFolder = pl.Path(__file__).parent.parent.parent.absolute()
    print(pathToMainFolder)
    pathToRealtimeData = os.path.join(pathToMainFolder, "data", "realtime")
    path = os.path.join(pathToRealtimeData, fileName + fileFormat)
    return path


def CreateMetricFulfillmentScore(path):
    final_score = []
    loc_score = []
    trf_score = []
    ccr_score = []
    mnf_score = []
    fd_score = []
    ic_score = []

    df = pd.read_csv(path, sep=",")
    print(df)

    # Create score for lines of code metric:
    for x in df.index:
        if df["linesOfCode"][x] <= 50:
            loc_score.append(0.06)

        elif df["linesOfCode"][x] > 50 and df["linesOfCode"][x] <= 100:
            loc_score.append(0.04)

        elif df["linesOfCode"][x] > 100 and df["linesOfCode"][x] <= 200:
            loc_score.append(0.02)
        else:
            loc_score.append(0)

    # Create scores
    for x in df.index:
        if df["testRequirementFulfilled"][x] == 0:
            trf_score.append(0)
        else:
            trf_score.append(0.08)

    for x in df.index:
        if df["cc_rank"][x] == "A":
            ccr_score.append(0.15)
        elif df["cc_rank"][x] == "B":
            ccr_score.append(0.12)
        elif df["cc_rank"][x] == "C":
            ccr_score.append(0.09)
        elif df["cc_rank"][x] == "D":
            ccr_score.append(0.06)
        elif df["cc_rank"][x] == "E":
            ccr_score.append(0.03)
        else:
            ccr_score.append(0)

    for x in df.index:
        if df["methodNameFulfillment"][x] == 0:
            mnf_score.append(0)
        else:
            mnf_score.append(0.33)

    for x in df.index:
        if df["functionalityDuplication"][x] == 0:
            fd_score.append(0.28)
        else:
            fd_score.append(0)

    for x in df.index:
        if df["inheritanceCheck"][x] >= 0 and df["inheritanceCheck"][x] <= 2:
            ic_score.append(0.10)
        elif df["inheritanceCheck"][x] > 2 and df["inheritanceCheck"][x] <= 4:
            ic_score.append(0.08)
        elif df["inheritanceCheck"][x] > 4 and df["inheritanceCheck"][x] <= 6:
            ic_score.append(0.06)
        elif df["inheritanceCheck"][x] > 6 and df["inheritanceCheck"][x] <= 8:
            ic_score.append(0.04)
        elif df["inheritanceCheck"][x] > 8 and df["inheritanceCheck"][x] <= 10:
            ic_score.append(0.02)
        else:
            ic_score.append(0)

    print(len(ccr_score), len(ic_score), len(
        fd_score), len(mnf_score), len(trf_score))

    # final score for the prediction output
    for x in df.index:
        print("Calculating final score")
        final_score.append(round(
            loc_score[x] + trf_score[x] + ccr_score[x] + mnf_score[x] + fd_score[x] + ic_score[x], 2))
        print("---------------------------------------------------------------------")
        print("FULFILLMENT OF REQUIREMENTS IS " + str(final_score[x]))

    print("---------------------------------------------------------------------")
    len(final_score)

    final_score[0:5]

    df["metricFulfillment"] = final_score

    df.head()

    print("----- New entry to codingBehavior.csv -----")
    print(df)

    print("DONE - Calculation successful.")
    print("---------------------------------------------------------------------")
