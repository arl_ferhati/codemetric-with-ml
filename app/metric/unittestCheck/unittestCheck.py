import os
import pathlib as pl
import os.path
import string
import exception.MetricExceptionHandling as exc


def getClassName():
    fileName = "/TestFileForMetric.cs"
    path = "C:/Users/arl14/Desktop/Code/masterthesis/codemetric-with-ml/data/test/" + fileName
    path_dir = os.path.dirname(path)
    file = open(path_dir + fileName, "r")

    fileRead = file.read()
    className = fileRead.split("class")[1].split()[0]
    print("Name of the class name is {}".format(className))
    global fileToRead
    fileToRead = fileRead
    return className


def isUnittestRequirementFulfilled():
    fileName = "/TestFileForMetric.cs"
    path = "C:/Users/arl14/Desktop/Code/masterthesis/codemetric-with-ml/data/test/" + fileName
    path_dir = os.path.dirname(path)
    className = getClassName()

    if isTestFileInCorrectNamespace(path_dir, className):
        return True
    return False


def isTestFileInCorrectNamespace(path, className):
    filesInDir = os.listdir(path)
    testClassName = className + "Tests.cs"
    if(testClassName in filesInDir):
        print("File is in incorrect namespace, please move test file to parent directory and into a 'Tests' folder...")
        return False
    print("First check fulfilled - The testfile is not in the src-folder ...")

    projectDir = os.path.dirname(path)
    testDir = projectDir + "/Test"
    checkIfDirExists = os.listdir(projectDir)
    if "Test" not in checkIfDirExists:
        #raise exc.TestFolderMissingException(projectDir)
        print("Test folder is missing.")
        return False

    filesInTestDir = os.listdir(testDir)
    if testClassName not in filesInTestDir:
        #raise exc.UnitTestMissingException(testClassName)
        print("Unit test for this class is missing!")
        return False
    print("Check done - Unit test {} does exist for the {} class".format(
        testClassName, className))
    return True


def getMethodName():
    # [0] is the part before the split and [-1] describes the last word inside the list of words.
    # Source: https://stackoverflow.com/questions/41228115/how-to-extract-the-first-and-final-words-from-a-string
    methodNamePartition = fileToRead.partition("(")[0]
    methodName = lastWord(methodNamePartition)
    print("The method name of the class is {}".format(methodName))
    return methodName


def lastWord(string):
    lis = list(string.split(" "))
    length = len(lis)
    return lis[length-1]


def tryFindFittingMethodName(methodName):
    fileName = "fileToAnalyze"
    fileFormat = ".txt"
    pathToMainFolder = pl.Path(__file__).parent.parent.parent.parent.absolute()
    print(pathToMainFolder)
    pathToRealtimeData = os.path.join(pathToMainFolder, "data", "realtime")
    path = os.path.join(pathToRealtimeData, fileName + fileFormat)
    print(path)
    with open(path) as f:
        print("Found file to analyze")
        file = f.readlines()
        filteredMethodNames = [s.replace("'", "").replace("[", "").replace(
            "]", "").replace(",", " ").replace("\n", "") for s in file]
        for filteredMethodName in filteredMethodNames:
            if filteredMethodName in methodName:
                print("A suitable method name was found")
                return True
        else:
            print("No suitable method name was found here")
            return False
