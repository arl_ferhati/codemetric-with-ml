# This metric gets the cyclomatic complexity of an class and checks if further commits have a range in that cyclomatic complexity.
# This can be used to get a model for a specific user or code base which can or should be followed.
# This could also lead to the fact to get a diagram of the code maybe via gauß chart
# This check could be used for a good visualization of the code.

def getCyclomaticComplexity():
    file = open(
        "C:/Users/arl14/Desktop/Code/masterthesis/codemetric-with-ml/data/test/TestFileForMetric.cs", "r")

    cyclomaticComplexity = 1

    for x in file:
        if x.__contains__("//"):
            continue
        if x.__contains__("if"):
            cyclomaticComplexity += 1
        if x.__contains__("||"):
            cyclomaticComplexity += 1
        if x.__contains__("&&"):
            cyclomaticComplexity += 1
        if x.__contains__("foreach"):
            cyclomaticComplexity += 1
        if x.__contains__("for"):
            cyclomaticComplexity += 1

    print("This class has a cyclomatic complexity of {}.".format(
        cyclomaticComplexity))

    cc_rank = getCyclomaticComplexityRank(cyclomaticComplexity)

    return cyclomaticComplexity, cc_rank


def getCyclomaticComplexityRank(cc_value):
    if 1 <= cc_value <= 5:
        return 'A'  # low risk - simple block
    if 6 <= cc_value <= 10:
        return 'B'  # low risk - well structured and stable block
    if 11 <= cc_value <= 20:
        return 'C'  # moderate risk - slightly complex block
    if 21 <= cc_value <= 30:
        return 'D'  # more than moderate risk - more complex block
    if 30 <= cc_value <= 40:
        return 'E'  # high risk - complex block, alarming
    if cc_value >= 41:
        return 'F'  # very high risk - error-prone, unstable block
