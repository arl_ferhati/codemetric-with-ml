import linesOfCodeCheck.linesOfCodeCheck as loc
import unittestCheck.unittestCheck as ut
import cyclomaticComplexityCheck.cyclomaticComplexityCheck as cc
import functionalityCheck.functionCheck as fc
import inheritanceCheck.inheritanceCheck as ic
import metricEvaluation as me
import csv
import pathlib as pl
import os.path

# This file calls metrics to write information from metrics into a separat file which can be analyzed by the user

header = ['className', 'linesOfCode',
          'testRequirementFulfilled', 'cyclometicComplexity', 'cc_rank', 'methodNameFulfillment', "functionalityDuplication", "inheritanceCheck"]
data = []

fileName = "codingBehavior"
fileFormat = ".csv"
pathToMainFolder = pl.Path(__file__).parent.parent.parent.absolute()
print(pathToMainFolder)
pathToRealtimeData = os.path.join(pathToMainFolder, "data", "realtime")
path = os.path.join(pathToRealtimeData, fileName + fileFormat)

className = ut.getClassName()
data.append(className)

linesOfCode = loc.getLinesOfCode()
data.append(linesOfCode)

testRequirementFulfilled = ut.isUnittestRequirementFulfilled()
if testRequirementFulfilled == True:
    testRequirementFulfilled = 1
else:
    testRequirementFulfilled = 0
data.append(testRequirementFulfilled)

cyclomaticComplexity = cc.getCyclomaticComplexity()
data.append(cyclomaticComplexity[0])
data.append(cyclomaticComplexity[1])

methodName = ut.getMethodName()
foundFittingMethodName = ut.tryFindFittingMethodName(methodName)
if foundFittingMethodName == True:
    foundFittingMethodName = 1
else:
    foundFittingMethodName = 0
data.append(foundFittingMethodName)

hasDuplicatedFunction = fc.HasDuplicatedFunction()
data.append(hasDuplicatedFunction)

depthOfInheritance = ic.ValueOfInheritance()
data.append(depthOfInheritance)
print("---------------------------------------------------------------------")
print("Static analysis finished.")

# Open file in write mode
with open(path, 'w', encoding='UTF8') as f:
    writer = csv.writer(f)  # Create the csv writer
    writer.writerow(header)
    writer.writerow(data)  # Change to writerows for multiple rows in csv file
    print("Create .csv file to inspect and analyze coding behavior...")

pathForMetric = me.GetActiveFileWithPath()
metricValue = me.CreateMetricFulfillmentScore(pathForMetric)
print("----- METRIC CALCULATION FINISHED -----")
