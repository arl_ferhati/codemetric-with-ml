This folder contains all the metrics to be considered. <br/>
The summary file is responsible for performing the verification of each check and determining a value between **0** and **1**. <br/>
In addition, a corresponding .csv file is written to document an overview of the user's coding behavior. <br/>
