def getLinesOfCode():
    file = open(
        "C:/Users/arl14/Desktop/Code/masterthesis/codemetric-with-ml/data/test/TestFileForMetric.cs", "r")

    logicalLinesOfCode = 0
    commentLinesOfCode = 0

    for x in file:
        if not x.isspace() and not x.__contains__("//"):
            logicalLinesOfCode += 1
        if x.__contains__("//"):
            commentLinesOfCode += 1
    print("This class has {} logical lines of code.".format(logicalLinesOfCode))
    print("This class has {} comment lines of code.".format(commentLinesOfCode))
    return logicalLinesOfCode
