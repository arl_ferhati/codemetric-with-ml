import ast

#code = ast.parse("print('Hello world!')")
# print(code)

path = "C:/Users/arl14/Documents/Universität/Masterthesis/Projekt/ExampleClass/src/GreetTestProgram.py"

file = open(path, "r").read()

t = ast.parse(file)
print(path)
# print(file)
print(t)

# It is also possible to see the AST which was formed for the above expression, just add this line with above script:
dump = ast.dump(t)
print(dump)
