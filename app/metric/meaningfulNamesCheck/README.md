# Meaningful names

In this metric, an appropriate name for the function is determined by machine learning based on the written source code.
The written code is checked for individual tokens and components via Abstract Syntax Tree (AST). The functionality is then matched in the vocabulary and the trained model makes various suggestions for a correct naming. However, these are percentage values and not complete values.
