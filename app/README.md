# Introduction

This contains the main code and the main functions which have to be used to check the code. <br/>
Here is the main application that calls the individual scripts to calculate the completeness of the metric.

# Code that learns from code

Here some examples of how ML can learn from source code, executables, and other code representations to make inferences about code:

- Detecting vulnerabilities in source code (Russell et. al., 2018; Sestili et. al., 2018)
- Patching source code (Chen et. al., 2018)
- Judging the maliciousness of an executable (Saxe and Berlin, 2015; Fleshman et. al., 2019)
- Identifying inputs that crash a program, or "fuzzing" (Cheng et. al., 2019)
- Automating reverse engineering (Katz et. al., 2019)
- Detecting design patterns (Zanoni et. al., 2015)
- Naming a function based on its source code (Alon et. al., 2019)
- Generating code based on natural language code descriptions (Yin and Neubig, 2017; Sun et. al., 2019)
