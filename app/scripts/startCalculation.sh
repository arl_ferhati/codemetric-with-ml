#!/bin/sh
python C:/Users/arl14/Desktop/Code/masterthesis/codemetric-with-ml/app/scripts/startCalculationTest.py
echo "The test for the sample script has been passed successfully"

echo "Starting the dynamic code analysis"
echo "---------------------------------------------------------------------"
echo "Lets start the machine learning part! This might take some time..."
echo "The prediction of the method name is starting now ..."
python C:/Users/arl14/Desktop/Code/masterthesis/codemetric-with-ml/app/metric/meaningfulNamesCheck/distributedCodeRepresentation.py --load C:/Users/arl14/Desktop/Code/masterthesis/codemetric-with-ml/data/savedModels/saved_model_iter8.release --predict
echo "Finished the prediction the method naming."

echo "---------------------------------------------------------------------"
echo "Lets start the static code analysis and use the previous metrics and calculations..."
python C:/Users/arl14/Desktop/Code/masterthesis/codemetric-with-ml/app/metric/summary.py
echo "---------------------------------------------------------------------"
